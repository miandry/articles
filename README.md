# Articles API service using NodeJS and Express JS with MVC structure #

This is a demo about api service for a blog 
### Install  ###

* Get source : git clone https://miandry@bitbucket.org/miandry/articles.git
* Install nodeJS look this url : https://nodejs.org/en
* Go to the folder : cd articles 
* Initial node js : npm install
* Start server :  npm start

### API LIST ###

* list all articles, 20 articles one page �> api/articles?page=1&pageSize=20 
* get an article content -> api/article/:id
* post an article (insert) -> api/post/article 
* list all comments of an article  -> api/comments/:id/article 
* list all replies of an comment  -> api/comments/:id/comment 
* comment on an article   -> api/comment/:id 
* reply on a comment   -> api/reply/:id 


### Article Data schema ###

* id -> integer auto increment
* nickname -> string required
* title -> string required
* content -> string required
* created -> Date


### Comment Data schema ###

* id -> integer auto increment
* parentId -> integer foreign key
* parentType -> string ['article' or 'comment']
* content -> string required
* created -> Date 

### Limitation of this demo  ###
* data persistence is not implement
* account system is not implement
