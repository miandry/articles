'use strict';

module.exports = function(router) {
    var articleController = require('../controllers/articleController');
    var commentController = require('../controllers/commentController');
    router.get("/articles",function(req,res){
          res.json(articleController.list(req,res));
    });
    router.get("/article/:articleId",function(req,res){
        res.json(articleController.read(req,res));
    });
    router.post("/post/article",function(req,res){
        res.json(articleController.insert(req,res));
    });

    router.get("/comments/:id/:parentType",function(req,res){
        res.json(commentController.list(req,res));
    });

    router.post("/comment/:id",function(req,res){
        res.json(commentController.addComment(req,res));
    });
    router.post("/reply/:id",function(req,res){
        res.json(commentController.reply(req,res));
    });


};
