'use strict';

exports.list = function(req,res) {
    var article = require('../models/article')
    var query = req.query ;
    return article.find(query)
};

exports.insert = function(req, res) {
    var article = require('../models/article')
    if( req.query.nickname
        && req.query.title
        && req.query.content
    ){
        var date_now = new Date();
        var fields =  {
                "nickname" : req.query.nickname,
                "title" : req.query.title,
                "content" : req.query.content,
                "created" : date_now
        }
        return article.set(fields);
    }else {
        return {"error" :"400","message" :"some fields required are missing"};
    }
};
exports.read = function(req, res) {
        var article = require('../models/article')
        return article.get(req.params.articleId);
};

