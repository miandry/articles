'use strict';


exports.list = function(req,res) {
    var comment = require('../models/comment')
    return comment.findByParent(req.params.id,req.params.parentType)
};
exports.addComment = function(req, res) {
    if(req.query.content){
        var date_now = new Date();
        var comment = require('../models/comment')
        var article = require('../models/article')
        if(article.get(req.params.id)){
            var fields =  {
                "parentId" : req.params.id,
                "parentType" : "article",
                "content" : req.query.content,
                "created" : date_now
            }
            return comment.set(fields);
        }else{
            return {"error" :"500","message" :"id not exist"};
        }
    } else {
        return {"error" :"400","message" :"some fields required are missing or incorrect"};
    }
};
exports.reply = function(req, res) {
    if(req.query.content){
        var date_now = new Date();
        var comment = require('../models/comment')
        if(comment.get(req.params.id)){
            var fields =  {
                "parentId" : req.params.id,
                "parentType" : "comment",
                "content" : req.query.content,
                "created" : date_now
            }
            return comment.set(fields);
        }else{
            return {"error" :"500","message" :"id not exist"};
        }
    } else {
        return {"error" :"400","message" :"some fields required are missing or incorrect"};
    }
};

