'use strict';

const articles = [];
exports.set = function(fields) {
    var number_items = articles.length ;
    articles.push({
        id: articles.length + 1 ,
        nickname: fields.nickname,
        title: fields.title,
        content: fields.content,
        created: fields.created
    })

    if((number_items+1) == articles.length  ){
        return {
            "message" : fields.title+ " saved successfully",
            "item":articles [articles.length-1]
        };
    }else{
        return {
            "error":"500", //Internal Server Error
            "message" : "error happen during saving item"
        };
    }

};
exports.get = function(id) {
    var item = null
    articles.forEach(function(element,index) {
        if(parseFloat(id) == parseFloat(index+1) ){
            item = (element);
        }
    });
    return item ;
};

exports.find = function(query) {

    //pageSize
    var page_size = 20 ;
    if(query.pageSize) {
        if(parseFloat(query.pageSize)==query.pageSize){
        page_size = query.pageSize
        }else{
            return {"error" :"400","message" :"pageSize should be numeric"};
        }
    }

    //Page
    var page = 0;
    if(query.page ){
        if(parseFloat(query.page)==query.page && query.page > 0){
          page = parseFloat(query.page) - 1
        }else{
            return {"error" :"400","message" :"pageSize should be numeric"};
        }
    }

    //get Current Items
    var offset = page_size*parseFloat(page)
    var limit = offset + parseFloat(page_size)
    var tempArticles =[];
    articles.forEach(function(element,index) {
        if(offset <= index && index < limit ){
            tempArticles.push(element);
        }
    });

    return {
        "items":tempArticles,
        "total":articles.length,
        "page": query.page,
        "pageSize" :query.pageSize
    };


};