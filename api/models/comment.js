'use strict';
const comments = [];

exports.set = function(fields) {
    var number_items = comments.length ;
    comments.push({
        id: comments.length + 1 ,
        parentType: fields.parentType,
        parentId : fields.parentId,
        content: fields.content,
        created: fields.created
    })
    if((number_items+1) == comments.length  ){
        return {
            "message" : " saved successfully",
            "comment_item":comments [comments.length-1]
        };
    }else{
        return {
            "error":"500", //Internal Server Error
            "message" : "error happen during saving item"
        };
    }
};
exports.get = function(id) {
    var item = null
    comments.forEach(function(element,index) {
        if(parseFloat(id) == parseFloat(index)){
            item = (element);
        }
    });
    return item ;
};
exports.findByParent = function(id,type){
    var tempComments =[];
    comments.forEach(function(element,index){
        if(id == element.parentId && type == element.parentType){
            tempComments.push(element);
        }
    });
    return tempComments;

};